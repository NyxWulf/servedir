package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"path/filepath"
)

var port = flag.Int("port", 8080, "Listen Port")

func main() {
	flag.Parse()
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal(err)
	}
	sPort := fmt.Sprintf(":%d", *port)

	fmt.Println("Serving", dir, "on port", sPort)
	log.Fatal(http.ListenAndServe(sPort, http.FileServer(http.Dir(dir))))
}
